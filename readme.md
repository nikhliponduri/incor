for starting app : -


                npm install (only for first time)
                npm start




admin credentials:-

username:- admin
password:- admin123


routes:-

POST /apartment :- for creating new aprtments for both users and admin

GET /apartment/appartmentId :- for getting specific apartment details if not admin only the                                      creator of the apartment can get the details


PUT /apartment/appartmentId :- for modifying apartment

DELETE /apartment/appartmentId :- for deleting apartment. admin can delete any apartment but user                                   can delete only his own apartment

GET /apartment :- admin will get all apartments list but user can get only his own apartments list



POST  /user/register :- for new user register can be done only by admin

POST  /user/admin/login :- for admin login

POST  /user/login :- for user login

DELETE /user/userID :- for deleting user account

PUT /user/userID :- for modifying user details

GET /user  :- to get all user accounts only for admin along with their apartments

GET /user/userId :- to get particular user details along with his apartments