import { ObjectId } from 'mongodb';
import { ObjectID } from 'mongodb';
import apartmentsModel from "../models/apartments.model";
import { sendResponse, serverError, badRequest } from "../utils/utils";
import usersModel from "../models/users.model";

//creating a new appartment
export const postCreateAppartment = async (req, res) => {
    try {
        const { body: { apartNo, floor, bedroomsize, creatorName, tenant = '' }, user: { id, role } } = req;
        // if role is admin he needs to provide a creatorName
        if (role === 'admin') {
            //checking for the creatorName in database
            var user = await usersModel.findOne({ username: creatorName });
            //if creator not found with given name return 400 status code
            if (!user) return badRequest(res, 'No user found with the name given as creator');
        }
        //create a new apartment model
        await new apartmentsModel({
            apartNo,
            bedroomsize,
            floor,
            creator: role === 'admin' ? user._id : id,
            owner: role === 'admin' ? 'admin' : id,
            tenant
        }).save();
        sendResponse(res);
    } catch (error) {
        serverError(res, error);
    }
}

// modify an existing apartment
export const putModifyApartment = async (req, res) => {
    try {
        const { body: { apartNo, floor, bedroomsize, tenant, creatorName }, user: { id, role }, params: { apId } } = req;
        // if it is not user check if the creator of apartment is user  if not return acess denied error
        if (role !== 'admin') {
            const apartment = await apartmentsModel.findById(apId);
            if (apartment.creator.toString() !== id) return badRequest(res, 'Not authorized', 401);
        }
        const details = {}
        // modify only required changes
        if (role === 'admin' && creatorName) {
            //check wether user exists
            const creator = await usersModel.findOne({ username: creatorName });
            if (!creator) return badRequest(res, 'No user found with that creator Name');
            details.creator = creator._id;
        }
        if (apartNo) {
            details.apartNo = apartNo;
        }
        if (floor) {
            details.floor = floor;
        }
        if (bedroomsize) {
            details.bedroomsize = bedroomsize;
        }
        if (tenant) {
            details.tenant = tenant;
        }
        await apartmentsModel.updateOne({ _id: ObjectId(apId) }, {
            $set: {
                ...details
            }
        });
        sendResponse(res);
    } catch (error) {
        serverError(res, error);
    }
}

export const deleteApartment = async (req, res) => {
    try {
        const { params: { apartId }, user: { role, id } } = req;
        //checking for an apartment with that id
        const apartment = await apartmentsModel.findById(apartId);
        if (!apartment) return badRequest(res, 'No apartment found with that id', 404);
        //if appartment creator is not same as the user return acess denied error
        if (role === 'user' && apartment.creator.toString() !== id) return badRequest(res, 'Not authorized', 401);
        await apartmentsModel.deleteOne({ _id: ObjectId(apartId) });
        sendResponse(res);
    } catch (error) {
        serverError(res, error);
    }
}

export const getAllAppartments = async (req, res) => {
    try {
        const { user: { id, role } } = req;
        // if admin get all apartments
        if (role === 'admin') {
            const apartments = await apartmentsModel.find({});
            return sendResponse(res, apartments);
        }
        //if user get only respective creator apartments
        else {
            const apartments = await apartmentsModel.find({ creator: ObjectID(id) });
            sendResponse(res, apartments);
        }
    } catch (error) {
        serverError(res, error)
    }
}

export const getApartmentDtail = async (req, res) => {
    try {
        const { user: { id, role }, params: { apartId } } = req;
        const apartment = await apartmentsModel.findById(apartId);
        if (!apartment) return badRequest(res, 'No apartment found with that id');
        //if not admin and creator is not user send 401 error code
        if (role === 'user' && apartment.creator.toString() !== id) return badRequest(res, 'un authorized', 401);
        sendResponse(res, apartment.toJSON());
    } catch (error) {
        serverError(res, error)
    }
}