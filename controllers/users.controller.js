import bcrypt from 'bcryptjs';
import { ObjectID } from 'mongodb';
import jwt from 'jsonwebtoken';
import { serverError, badRequest, sendResponse, isValidObjectId } from "../utils/utils"
import usersModel from "../models/users.model";
import { SECRETKEY, ADMIN_NAME, ADMIN_PASSWORD } from '../utils/constants';

//logging in a user
export const postLoginUser = async (req, res) => {
    try {
        const { body: { username, password } } = req;
        const user = await usersModel.findOne({ username });
        //if user already exists with username
        if (!user) return badRequest(res, "no user found with that name");
        const isValid = await bcrypt.compare(password, user.password);
        //if password dosent matches send 400 error
        if (!isValid) return badRequest(res, 'Invalid credentials');
        jwt.sign({ id: user._id, role: 'user' }, SECRETKEY, (err, token) => {
            if (err) {
                return serverError(res, err);
            }
            return sendResponse(res, token);
        })
    } catch (error) {
        serverError(res, error)
    }
}

//admin login
export const adminLogin = async (req, res) => {
    try {
        const { body: { username = '', password = '' } } = req;
        //checking if credentials are correct
        if (username !== ADMIN_NAME || password !== ADMIN_PASSWORD) return badRequest(res, 'Invalid credentials');
        jwt.sign({ id: ADMIN_NAME, role: 'admin' }, SECRETKEY, (err, token) => {
            if (err) {
                return serverError(res, err);
            }
            return sendResponse(res, token);
        })
    } catch (error) {
        serverError(res, error);
    }
}

// registering a new user
export const postRegisterUser = async (req, res) => {
    try {
        const { body: { username, password } } = req;
        const user = await usersModel.findOne({ username });
        //checking if user already exists with that user name
        if (user) return badRequest(res, 'user already exists with that name');
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(password, salt);
        await new usersModel({
            username,
            password: hash
        }).save();
        sendResponse(res);
    } catch (error) {
        serverError(res, error);
    }
}

//deleting a user
export const deleteUser = async (req, res) => {
    try {
        const { params: { userId } } = req;
        await usersModel.findByIdAndDelete(userId);

        sendResponse(res);
    } catch (error) {
        serverError(res, error)
    }
}

//changing user details
export const putModifyUser = async (req, res) => {
    try {
        const { body: { username }, params: { userId } } = req;
        const user = await usersModel.findOne({ username });
        if (user) return badRequest(res, 'User already exists with that name');
        await usersModel.updateOne({ _id: ObjectID(userId) }, {
            $set: {
                username
            }
        });
        sendResponse(res);
    } catch (error) {
        serverError(res, error);
    }
}

//get all users data
export const getAllUsers = async (req, res) => {
    try {
        //sending users details along with their apartments
        const users = await usersModel.aggregate([
            {
                $match: {}
            },
            {
                $lookup: {
                    from: 'apartments',
                    localField: '_id',
                    foreignField: 'creator',
                    as: 'apartments',
                }
            },
            {
                $project: {
                    password: 0
                }
            }
        ]);
        sendResponse(res, users);
    } catch (error) {
        serverError(res, error)
    }
}

//get particular user detials
export const getUserDetails = async (req, res) => {
    try {
        const { params: { userId } } = req;
        if (!isValidObjectId(userId)) return badRequest(res, '', 404);
        //sending user detils along with their apartment details
        const user = await usersModel.aggregate([
            { $match: { _id: ObjectID(userId) } },
            {
                $lookup: {
                    localField: '_id',
                    foreignField: 'creator',
                    as: 'apartments',
                    from: 'apartments'
                }
            },
            {
                $project: {
                    password: 0
                }
            }
        ]);
        sendResponse(res, user[0]);
    } catch (error) {
        serverError(res, error)
    }
}