import validator, { PASSWORD, NUMERIC, ALPHA_NUMERIC } from "./validator";
import { badRequest, isValidObjectId } from "../utils/utils";

export const isValidRegisterForm = (req, res, next) => {
    const { body: { username = '', password = '' } } = req;

    //validating form details
    const options = [
        { key: 'username', value: username, type: ALPHA_NUMERIC, min: 3, max: 30 },
        { key: 'password', value: password, type: PASSWORD }
    ]
    const validation = validator(options);
    //if form detils are missing send 400 error along with details
    if (!validation.isValidForm) return badRequest(res, validation.errors);
    next();
}

export const isValidLoginForm = (req, res, next) => {
    const { body: { username = '', password = '' } } = req;
    const options = [
        { key: 'username', value: username, type: ALPHA_NUMERIC, min: 3, max: 30 },
        { key: 'password', value: password, type: ALPHA_NUMERIC, min: 1 }
    ]
    const validation = validator(options);
    if (!validation.isValidForm) return badRequest(res, validation.errors);
    next();
}

export const isValidModifyUserForm = (req, res, next) => {
    const { body: { username = '' }, params: { userId } } = req;
    const options = [
        { key: 'username', value: username, type: ALPHA_NUMERIC, min: 3, max: 30 }
    ]
    const validation = validator(options);
    if (!isValidObjectId(userId)) {
        validation.errors.userId = 'Invalid param';
        validation.isValidForm = false
    }
    if (!validation.isValidForm) return badRequest(res, validation.errors);
    next();
}

export const isValidPostApartmentForm = (req, res, next) => {
    const { body: { apartNo = '', floor = '', bedroomsize, creatorName, tenant = '' }, user: { role } } = req;
    const options = [
        { key: 'apartNo', value: apartNo, type: ALPHA_NUMERIC, min: 3 },
        { key: 'floor', value: floor, type: NUMERIC },
        { key: 'bedroomsize', type: NUMERIC, value: bedroomsize }
    ]
    if (role === 'admin') {
        options.push({ key: 'creatorName', type: ALPHA_NUMERIC, value: creatorName, min: 1 });
    }
    if (tenant) {
        options.push({ key: 'tenant', type: ALPHA_NUMERIC, value: tenant, min: 1 });
    }
    const validation = validator(options);
    if (!validation.isValidForm) return badRequest(res, validation.errors);
    next();
}

export const isValidPutApartmentForm = (req, res, next) => {
    const { body: { apartNo = '', floor = '', bedroomsize = '', creatorName = '', tenant = '' }, user: { role } } = req;
    const options = []
    if (apartNo) {
        options.push({ key: 'aprtNo', value: apartNo, type: ALPHA_NUMERIC, min: 3 });
    }
    if (floor) {
        options.push({ key: 'floor', value: floor, type: NUMERIC })
    }
    if (bedroomsize) {
        options.push({ key: 'bedroomSize', type: NUMERIC, value: bedroomsize })
    }
    if (role === 'admin' && creatorName) {
        options.push({ key: 'creatorName', type: ALPHA_NUMERIC, value: creatorName, min: 1 })
    }
    if (tenant) {
        options.push({ key: 'tenant', type: ALPHA_NUMERIC, value: tenant, min: 1 })
    }
    const validation = validator(options);
    if (!validation.isValidForm) return badRequest(res, validation.errors);
    next();
}