import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import mongoose from 'mongoose';
import userRouter from './routes/users.router';
import apartMentRouter from './routes/apartments.router';


var app = express();


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

mongoose.connect('mongodb://localhost/incor', { useNewUrlParser: true }).catch(err => {
  console.log('Error connecting to databse', err);
});

// redirecting the routes

app.use('/user', userRouter);
app.use('/apartment', apartMentRouter);

// catch 404 
app.use('*', (req, res) => {
  res.status(404).end();
});

process.on('uncaughtException', err => {
  console.log(err)
});

process.on('unhandledRejection', err => {
  console.log(err);
})

module.exports = app;
