import { Router } from 'express';
import { isValidLoginForm, isValidRegisterForm, isValidModifyUserForm } from '../validation/validations';
import { postLoginUser, postRegisterUser, getAllUsers, getUserDetails, deleteUser, adminLogin, putModifyUser } from '../controllers/users.controller';
import { isAdmin } from '../auth';

const userRouter = Router();

//user login
userRouter.post('/login', isValidLoginForm, postLoginUser);

//change user detils
userRouter.put('/:userId', isAdmin, isValidModifyUserForm, putModifyUser)

//register user
userRouter.post('/register', isValidRegisterForm, postRegisterUser);

//get all users details
userRouter.get('/', isAdmin, getAllUsers);

//get a particular user details with userId
userRouter.get('/:userId', isAdmin, getUserDetails);

//delete a user
userRouter.delete('/:userId', isAdmin, deleteUser);

//admin login route
userRouter.post('/admin/login', isValidLoginForm, adminLogin);

export default userRouter