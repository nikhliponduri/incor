import { Router } from 'express';
import { validateUser } from '../auth';
import { postCreateAppartment, putModifyApartment, deleteApartment, getAllAppartments, getApartmentDtail, } from '../controllers/apartments.controller';
import { isValidPostApartmentForm, isValidPutApartmentForm } from '../validation/validations';

const apartMentRouter = Router();

// get apartments list
apartMentRouter.get('/', validateUser, getAllAppartments);

//get apartment details
apartMentRouter.get('/:apartId', validateUser, getApartmentDtail);

// create a new apartment
apartMentRouter.post('/', validateUser, isValidPostApartmentForm, postCreateAppartment);

//modify existing apartment
apartMentRouter.put('/:apId', validateUser, isValidPutApartmentForm, putModifyApartment);

//delete an apartment
apartMentRouter.delete('/:apartId', validateUser, deleteApartment);

export default apartMentRouter;