import { Schema, model } from 'mongoose';

const apartmentSchema = new Schema({
    apartNo: {
        type: String,
        required: true,
    },
    floor: {
        type: Number,
        required: true
    },
    bedroomsize: {
        type: Number,
        required: true
    },
    creator: {
        type: Schema.Types.ObjectId,
        index: true
    },
    owner: {
        type: String,
    },
    tenant: {
        type: String,
    }
});

export default model('apartments', apartmentSchema);